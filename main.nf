params.files="/mount/*"

/*files=Channel.fromPath("data/*")
files.view { "value: $it" }*/


process showContent{
	echo true
	publishDir '/mount/path/nfout/', mode: 'copy', overwrite: true
	input:
	val path from params.files
	output:
	file "rtest.txt" into  folderContent
	
	"""
	ls $path >rtest.txt
	"""
}


process executeStrangeCommand{
	publishDir '/mount/path/nfout/', mode: 'copy', overwrite: true
	echo true
	input:
	val path from params.files
	val sampleType from params.sampletype
	output:
	file "StrangeComandOutput.txt" into  strangOutput
	
	"""
	strangeCommand /mount/path/ > StrangeComandOutput.txt
	echo "**********************************\n" >> StrangeComandOutput.txt
	echo $sampleType >>StrangeComandOutput.txt
	"""
}
